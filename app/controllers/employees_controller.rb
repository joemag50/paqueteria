# Controller Employee
class EmployeesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_model

  def index
    @employees = Employee.all.order(:last_name)
  end

  def nomina
    @employees = Employee.all.order(:last_name)
  end

  def new
  end

  def edit
  end
  
  def create
    employee = Employee.new employee_params
    if employee.save
      flash[:notice] = 'Empleado creado'
      redirect_to employees_path
    else
      flash[:alert] = employee.errors
      redirect_to new_employee_path
    end
  end

  def update
    if @employee.update_attributes employee_params
      flash[:notice] = 'Empleado actualizado'
      redirect_to employees_path
    else
      flash[:alert] = @employee.errors
      redirect_to new_employee_path
    end
  end
  
  def destroy
    if @employee.destroy
      flash[:notice] = 'Empleado eliminado'
      redirect_to employees_path
    else
      flash[:alert] = @employee.errors
      redirect_to edit_employee_path
    end
  end
  
  private
  def find_model
    @employee = Employee.find(params[:id]) if params[:id]
  end

  def employee_params
    params.require(:employee).permit(:name, :last_name, :birth,
                                     :pay_rate, :email, :phone,
                                     :residence)
  end
end
