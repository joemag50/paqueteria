# Controller Clients
class RegistrationClientsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_model

  def index
    @clients = Client.all.order(:email)
  end

  def show
  end
  
  def new
  end

  def edit
  end
  
  def create
    client = Client.new client_params
    if client.save
      flash[:notice] = 'Cliente creado'
      redirect_to registration_clients_path
    else
      flash[:alert] = client.errors
      redirect_to new_registration_client_path
    end
  end

  def modal_create
    @client = Client.new client_params
    if @client.save
      render json: @client
    else
      render json: { errors: @client.errors }
    end
  end

  def json_client
    @client = Client.find(params[:id])
    if @client
      render json: @client
    else
      render json: { errors: 'Not found' }
    end
  end

  def update
    if @client.update_attributes client_params
      flash[:notice] = 'Cliente actualizado'
      redirect_to registration_clients_path
    else
      flash[:alert] = @client.errors
      redirect_to edit_registration_client_path
    end
  end
  
  def destroy
    if @client.destroy
      flash[:notice] = 'Cliente eliminado'
      redirect_to registration_clients_path
    else
      flash[:alert] = @client.errors
      redirect_to edit_registration_client_path
    end
  end

  def mis_paquetes
    @packs = Pack.all.where(client_id: current_client.id)
  end

  private
  def find_model
    @client = Client.find(params[:id]) if params[:id]
  end

  def client_params
    params.require(:client).permit(:email, :password, :phone, :residence, :name, :last_name)
  end
end
