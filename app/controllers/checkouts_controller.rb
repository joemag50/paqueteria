class CheckoutsController < ApplicationController
  def index
    @clients = Client.all
  end

  def save_checkout
    ticket = Pack.get_ticket
    params["pack"].each { |p|
      Pack.create(ticket: ticket + 1,
                  client_id: params["client"],
                  addressee: p[1]["addressee"],
                  address_state: p[1]["address_state"],
                  address_city: p[1]["address_city"],
                  address_pc: p[1]["address_pc"],
                  address_street: p[1]["address_street"],
                  size_x: p[1]["size_x"],
                  size_y: p[1]["size_y"],
                  size_z: p[1]["size_z"],
                  weight: p[1]["weight"],
                  description: p[1]["description"],
                  status: 0)
    }
    redirect_to view_checkout_path
  end

  def view_checkout
    @last_ticket = Pack.last_ticket
  end

  def confirm_checkout
  end
end
