# Controller Packs
class PacksController < ApplicationController
  before_action :authenticate_user!
  before_action :find_model

  def index
    @packs = Pack.all.order(:address_state)
  end

  def new
  end

  def edit
  end
  
  def create
    pack = Pack.new pack_params
    if pack.save
      flash[:notice] = 'Paquete creado'
      redirect_to packs_path
    else
      flash[:alert] = pack.errors
      redirect_to new_pack_path
    end
  end

  def update
    if @pack.update_attributes pack_params
      flash[:notice] = 'Paquete actualizado'
      redirect_to packs_path
    else
      flash[:alert] = @pack.errors
      redirect_to edit_pack_path
    end
  end
  
  def destroy
    if @pack.destroy
      flash[:notice] = 'Paquete eliminado'
      redirect_to packs_path
    else
      flash[:alert] = @pack.errors
      redirect_to edit_pack_path
    end
  end

  private
  def find_model
    @pack = Pack.find(params[:id]) if params[:id]
  end

  def pack_params
    params.require(:client).permit(:client_id, :addressee, :address_state,
      :address_city, :address_pc, :address_street,
      :size_x, :size_y, :size_z, :weight, :priority)
  end
end
 