#Controller Rutas
class RutasController < ApplicationController
  before_action :authenticate_user!
  before_action :find_model

  def index
    @rutas = Rutum.all
  end

  def new
  end

  def edit
  end

  def create
    @ruta = Rutum.new ruta_params
    if @ruta.save
      flash[:notice] = 'Ruta creado'
      redirect_to rutas_path
    else
      flash[:alert] = @ruta.errors
      redirect_to new_ruta_path
    end
  end

  def update
    if @ruta.update_attributes ruta_params
      flash[:notice] = 'Ruta actualizado'
      redirect_to rutas_path
    else
      flash[:alert] = @ruta.errors
      redirect_to new_ruta_path
    end
  end

  def destroy
    @ruta.destroy
    redirect_to rutas_path
  end

  def select_vehiculo
    @vehiculos = Vehiculo.all
  end

  def confirm_vehiculo
    cookies[:vehiculo_id] = params[:vehiculo_id]
    redirect_to select_packs_path
  end

  def select_packs
    @paquetes = Pack.all.capturado
  end

  def confirm_packs
    cookies[:packs] = params[:packs]
    redirect_to select_ruta_path
  end

  def select_ruta
    @rutas = Rutum.all
  end

  def confirmar_rutas
    @trip = Trip.create({vehiculo_id: cookies[:vehiculo_id], pack_id: cookies[:packs].split('&').map(&:to_i), ruta_id: params[:ruta_id], status: 0 })
    Trip.last.pack_id.each { |x|
      Pack.find(x).update_attribute(:status, 1)
    }
    redirect_to index_rutas_path
  end

  def index_rutas
    @trips = Trip.all
  end

  private
  def find_model
    @ruta = Rutum.find(params[:id]) if params[:id]
  end

  def ruta_params
    params.require(:ruta).permit(:emisor, :destino, :distancia)
  end
end
