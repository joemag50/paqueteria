class ApplicationController < ActionController::Base

  private
  def authenticate_user!
    unless current_user.present? || current_client.present?
      redirect_to root_path
    end
  end
end
