class ReportesController < ApplicationController
  def reportes
  end

  def clientes
    @clientes = Client.all.order(:id)
    respond_to do |format|
      format.csv do
        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename=clientes.csv'
      end
    end
  end

  def usuarios
    @usuarios = User.all.order(:id)
    respond_to do |format|
      format.csv do
        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename=usuarios.csv'
      end
    end
  end

  def empleados
    @empleados = Employee.all.order(:id)
    respond_to do |format|
      format.csv do
        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename=empleados.csv'
      end
    end
  end
end
