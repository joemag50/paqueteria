#Controlador Vehiculos
class VehiculosController < ApplicationController
  before_action :authenticate_user!
  before_action :find_model

  def index
    @vehiculos = Vehiculo.all
  end

  def new
  end

  def edit
  end

  def create
    @vehiculo = Vehiculo.new vehiculo_params
    if @vehiculo.save
      flash[:notice] = 'Vehiculo creado'
      redirect_to vehiculos_path
    else
      flash[:alert] = @vehiculo.errors
      redirect_to new_vehiculo_path
    end
  end

  def update
    if @vehiculo.update_attributes vehiculo_params
      flash[:notice] = 'Vehiculo creado'
      redirect_to vehiculos_path
    else
      flash[:alert] = @vehiculo.errors
      redirect_to new_vehiculo_path
    end
  end

  def destroy
    if @vehiculo.destroy
      flash[:notice] = 'Vehiculo eliminado'
      redirect_to vehiculos_path
    else
      flash[:alert] = @vehiculo.errors
      redirect_to edit_vehiculo_path
    end
  end

  private
  def find_model
    @vehiculo = Vehiculo.find(params[:id]) if params[:id]
  end

  def vehiculo_params
    params.require(:vehiculo).permit(:marca,:modelo,:max_volumen,:max_peso)
  end
end
