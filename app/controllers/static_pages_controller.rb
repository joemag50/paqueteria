class StaticPagesController < ApplicationController
  def home
    if user_signed_in? || client_signed_in?
      redirect_to admin_home_path
    end
  end

  def who_we_are
  end

  def services
  end

  def contact
  end
end
