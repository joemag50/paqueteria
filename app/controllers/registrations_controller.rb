class RegistrationsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_model

  def index
    @users = User.all
  end
  
  def show
  end
  
  def new
    @employees = Employee.without_user.concat_last_name
  end

  def edit
    @employees = Employee.without_user.concat_last_name
  end

  def update
    if @user.update_attributes user_params
      redirect_to registrations_path
    else
      flash[:alert] = @user.errors.messages
      redirect_to new_registration_path
    end
  end

  def create
    @user = User.new user_params
    if @user.save
      redirect_to registrations_path
    else
      flash[:alert] = @user.errors.messages
      redirect_to new_registration_path
    end
  end

  def destroy
    if @user.destroy
      redirect_to registrations_path
    else
      flash[:alert] = @user.errors.messages
      redirect_to registrations_path
    end
  end

  private
  def find_model
    @user = User.find(params[:id]) if params[:id]
  end

  def user_params
    params.require(:user).permit(:email, :password, :role, :employee_id)
  end
end