class ContactController < ApplicationController
  def index
    @contacts = Contact.all
  end

  def show
  	@contact = Contact.find(params[:id])
  end

  def new
  end

  def create
    @contact = Contact.new contact_params
    if @contact.save
      flash[:notice] = 'Se ha enviado su mensaje'
      redirect_to root_path
    else
      flash[:alert] = @contact.errors
      redirect_to contacto_path
    end
  end

  private

  def contact_params
  	params.require(:contact).permit(:name, :email, :comment)
  end
end
