class Pack < ApplicationRecord
  enum status: [:capturado, :asignado, :transcurso, :llegada]

  scope :last_ticket, -> { where(ticket: Pack.get_ticket) }
  
  def self.get_ticket
    return Pack.last.ticket || 0
  end
end
