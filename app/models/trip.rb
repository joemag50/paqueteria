class Trip < ApplicationRecord
  enum status: [:preparado, :transcurso, :entregado, :accidente]
end
