class Client < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable, :validatable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable

  validates :email, presence: :true, uniqueness: :true
  validates :password, presence: true, if: lambda { new_record? || !password.blank? }
  validates :password, presence: true, on: create
end
