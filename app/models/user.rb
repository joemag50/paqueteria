class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable, :validatable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable
         
  enum role: %i[root admin super operator cashier]
  validates :employee_id, presence: true, uniqueness: true
  validates :email, presence: :true, uniqueness: :true
  #validates :password, presence: true, if: lambda { new_record? || !password.blank? }
  #validates :password, presence: true, on: create
  belongs_to :employee
end
