# Model Employee
class Employee < ApplicationRecord
  validates :name, :last_name, :birth, :pay_rate, :residence, :email, presence: true
  validates :email, presence: :true, uniqueness: :true

  scope :without_user, -> { joins('LEFT JOIN users s ON employees.id = s.employee_id WHERE s.employee_id IS NULL ') }
  scope :concat_last_name, -> { pluck("initcap(employees.last_name) || ' ' || initcap(employees.name), employees.id") }
end
