$(document).on('employees#nomina:loaded', function() {
  $('input[type="number"]').on('change', function() {
    var id = $(this).attr('id').split('_')[2];
    var tabulacion = $(this).data('tabulation');
    var dias = $(this).val();
    var pago = tabulacion * dias;
    $('#nomina_pay_' + id).text(pago.toFixed(1));
  });
});
