// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on('turbolinks:load', function() {
  $("#new_pack_form").on('keyup', function() {
    var $emptyFields = $(this).find('input').filter(function() {
      return $.trim(this.value) === "";
    });
    if (!$emptyFields.length) {
      $('#guardar_paquete').removeClass('disabled');
    } else {
      $('#guardar_paquete').addClass('disabled');
    }
  });

  $('#new_pack_form').find(':input[type="number"]').on('keyup', function() {
    if (isNaN(parseInt($(this).val()))) {
      $($(this).parent()).addClass('has-danger');
    } else {
      $($(this).parent()).removeClass('has-danger');
    }
  });

  $('#save_checkout').on('click', function(e) {
    var data = [];
    $('#table-rows').find("tr.data").map(function (index, elem) {
      var ret = [];
      $('.inputValue', this).each(function () {
        var d = $(this).val()||$(this).text();
        ret.push(d);
      });
      data[index] = ret;
    });
    
    var packs = [];
    for (i = 0; i < data.length; i++) {
      packx = {
        addressee : [],
        address_street : [],
        address_city : [],
        address_pc : [],
        address_state : [],
        description : [],
        size_x : [],
        size_y : [],
        size_z : [],
        weight : []
      };
      packx["description"] = data[i][0];
      size = data[i][1].replace("[", "").replace("]", "").replace(/cm/g, "").replace(/ /g, "");
      packx["size_x"] = size.split(",")[0];
      packx["size_y"] = size.split(",")[1];
      packx["size_z"] = size.split(",")[2];
      packx["weight"] = data[i][2].replace("kg", "");
      packx["addressee"] = data[i][3];
      address = data[i][4].split(", ");
      packx["address_street"] = address[0];
      packx["address_city"] = address[1];
      packx["address_pc"] = address[2];
      packx["address_state"] = address[3];
      packs[i] = packx;
    }

    var info = {
      pack: packs,
      client: $('#card_folio_cliente').text()
    };

    $.ajax({
      method: 'POST',
      url: $(this).attr("action"),
      data: info,
      success: function (response) {
        console.log(response);
      },
      error: function (response) {
        console.log(response);
      }
    });
  });

  $('#guardar_paquete').on('click', function (e) {
    e.preventDefault();

    $row = $('<tr class="data"></tr>');
    $form = $('#new_pack_form');

    var d = $($form).find('#pack_description').val();
    var x = $($form).find('#pack_size_x').val();
    var y = $($form).find('#pack_size_y').val();
    var z = $($form).find('#pack_size_z').val();
    var w = $($form).find('#pack_weight').val();
    var k = $($form).find('#pack_addressee').val();
    var s = $($form).find('#pack_address_street').val();
    var c = $($form).find('#pack_address_city').val();
    var f = $($form).find('#pack_address_pc').val();
    var l = $($form).find('#pack_address_state').val();

    $row.append($('<th class="inputValue"></th>').text(d));
    $row.append($('<td class="inputValue"></td>').text("[" + x + " cm, " + y + " cm, " + z + " cm]"));
    $row.append($('<td class="inputValue"></td>').text( w + " kg"));
    $row.append($('<td class="inputValue"></td>').text(k));
    $row.append($('<td class="inputValue"></td>').text(s + ", " + c + ", " + f + ", " + l));
    $('#table-rows').append($row);

    $('#paquete').modal('hide');
  });

  $('#button_select').on('click', function (e) {
    nombre = $('#nombre_cliente').text();
    residencia = $('#residencia_cliente').text();
    folio = $('#folio_cliente').text();
    if (nombre == "" || residencia == "")
      return;
    
    $('#card_nombre_cliente').text(nombre);
    $('#card_residencia_cliente').text(residencia);
    $('#card_folio_cliente').text(folio);
    $('#cliente').modal('toggle');
  });

  $('#select-beast').selectize({
    create: function(input, callback) {
      $('#form_client').modal('show');
      $('#client_email').val(input);
      $('#new_client_form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
          method: 'POST',
          url: $(this).attr("action"),
          data: $(this).serialize(),
          success: function (response) {
            callback({ value: response.id, text: response.email});
            $('#form_client').modal('toggle');
          },
          error: function (response) {
            console.log(response);
          }
        });
      });
    },
    sortField: 'text',
    onChange: function(value, isOnInitialize) {
      $.ajax({
        method: 'GET',
        url: "/json_client/" + value,
        data: $(this).serialize(),
        success: function (response) {
          $('#nombre_cliente').text(response.name + ' ' + response.last_name);
          $('#residencia_cliente').text(response.residence);
          $('#telefono_cliente').text(response.phone);
          $('#folio_cliente').text(response.id);
        },
        error: function (response) {
          console.log(response);
        }
      });
    }
  });
});
