// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on('rutas#select_ruta:loaded', function() {
  $('input[type="checkbox"]').on('click', function() {
    var id = $(this).attr('id');
    $('input[type="checkbox"]').each(function() {
      $(this).prop('checked', false);
      if (id == $(this).attr('id'))
      {
        $(this).prop('checked', true);
      }
    });
  });
  $('#confirmar_rutas').on('click', function (e) {
    e.preventDefault();
    var selected;
    $('input[type="checkbox"]').each(function() {
      if ($(this).prop('checked'))
      {
        selected = $(this).attr('id').split('_')[2];
      }
    });
    if (selected === undefined) {
      alert('Selecciona una ruta');
      return;
    }
    var info = {
      ruta_id: selected
    }
    $.ajax({
      method: 'POST',
      url: confirmar_rutas,
      data: info,
      success: function (response) {
        console.log(response);
      },
      error: function (response) {
        console.log(response);
      }
    });
  });
});

$(document).on('rutas#select_packs:loaded', function() {
  $('#confirmar_packs').on('click', function (e) {
    e.preventDefault();
    var selected = [];
    $('input[type="checkbox"]').each(function() {
      if ($(this).prop('checked'))
      {
        selected.push($(this).attr('id').split('_')[2]);
      }
    });
    if (selected.length == 0) {
      alert('Selecciona los paquetes');
      return;
    }
    var info = {
      packs: selected
    }
    $.ajax({
      method: 'POST',
      url: confirm_packs,
      data: info,
      success: function (response) {
        console.log(response);
      },
      error: function (response) {
        console.log(response);
      }
    });
  });
});

$(document).on('rutas#select_vehiculo:loaded', function() {
  $('input[type="checkbox"]').on('click', function() {
    var id = $(this).attr('id');
    $('input[type="checkbox"]').each(function() {
      $(this).prop('checked', false);
      if (id == $(this).attr('id'))
      {
        $(this).prop('checked', true);
      }
    });
  });

  $('#confirm_vehiculo').on('click', function (e) {
    e.preventDefault();
    var selected;
    $('input[type="checkbox"]').each(function() {
      if ($(this).prop('checked'))
      {
        selected = $(this).attr('id').split('_')[2];
      }
    });
    if (selected === undefined) {
      alert('Selecciona un vehiculo');
      return;
    }
    var info = {
      vehiculo_id: selected
    }
    $.ajax({
      method: 'POST',
      url: confirm_vehiculo,
      data: info,
      success: function (response) {
        console.log(response);
      },
      error: function (response) {
        console.log(response);
      }
    });
  });
});