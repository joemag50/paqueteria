Rails.application.routes.draw do

  resources :contact

  get 'reportes/reportes'
  get 'reportes/clientes'
  get 'reportes/usuarios'
  get 'reportes/empleados'

  get 'registration_clients/mis_paquetes'

  get 'checkouts/index'
  post :save_checkout, to: 'checkouts#save_checkout'
  get :view_checkout, to: 'checkouts#view_checkout'
  post :confirm_checkout, to: 'checkouts#confirm_checkout'

  devise_for :clients, controllers: { sessions: 'clients/sessions', 
                                      registrations: 'clients/registrations',
                                      confirmations: 'clients/confirmations',
                                      passwords: 'clients/passwords' }

  devise_for :users, controllers: { sessions: 'users/sessions', 
                                    registrations: 'users/registrations',
                                    confirmations: 'users/confirmations',
                                    passwords: 'users/passwords' }

  root to: 'static_pages#home'
  get :admin_home, to: 'admin#home'

  #Paginas estaticas
  get :quienes_somos, to: 'static_pages#who_we_are'
  get :servicios, to: 'static_pages#services'
  get :contacto, to: 'static_pages#contact'

  resources :registrations
  resources :employees
  resources :registration_clients
  resources :packs
  resources :vehiculos
  resources :rutas

  get :select_vehiculo, to: 'rutas#select_vehiculo'
  post :confirm_vehiculo, to: 'rutas#confirm_vehiculo'

  get :select_packs, to: 'rutas#select_packs'
  post :confirm_packs, to: 'rutas#confirm_packs'

  get :select_ruta, to: 'rutas#select_ruta'
  post :confirmar_rutas, to: 'rutas#confirmar_rutas'

  get :index_rutas, to: 'rutas#index_rutas'

  post :client_modal_create, to: 'registration_clients#modal_create'
  get '/json_client/(:id)', to: 'registration_clients#json_client'

  get :nomina, to: 'employees#nomina'
end
