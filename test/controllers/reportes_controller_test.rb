require 'test_helper'

class ReportesControllerTest < ActionDispatch::IntegrationTest
  test "should get clientes" do
    get reportes_clientes_url
    assert_response :success
  end

  test "should get usuarios" do
    get reportes_usuarios_url
    assert_response :success
  end

  test "should get empleados" do
    get reportes_empleados_url
    assert_response :success
  end

end
