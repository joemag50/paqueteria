require 'test_helper'

class VehiculosControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get vehiculos_index_url
    assert_response :success
  end

  test "should get new" do
    get vehiculos_new_url
    assert_response :success
  end

  test "should get edit" do
    get vehiculos_edit_url
    assert_response :success
  end

  test "should get create" do
    get vehiculos_create_url
    assert_response :success
  end

  test "should get update" do
    get vehiculos_update_url
    assert_response :success
  end

  test "should get destroy" do
    get vehiculos_destroy_url
    assert_response :success
  end

end
