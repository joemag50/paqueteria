require 'test_helper'

class RutasControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get rutas_index_url
    assert_response :success
  end

  test "should get new" do
    get rutas_new_url
    assert_response :success
  end

  test "should get edit" do
    get rutas_edit_url
    assert_response :success
  end

  test "should get create" do
    get rutas_create_url
    assert_response :success
  end

  test "should get update" do
    get rutas_update_url
    assert_response :success
  end

  test "should get destroy" do
    get rutas_destroy_url
    assert_response :success
  end

end
