# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Employees
5.times do
  email = "#{Faker::Lorem.word}@#{Faker::Lorem.word}.com"
  e = Employee.create(name: Faker::Lorem.word,
                      last_name: Faker::Lorem.word,
                      birth: Time.now,
                      pay_rate: Faker::Number.number(4),
                      email: email,
                      phone: Faker::Number.number(10).to_s,
                      residence: Faker::Lorem.word)

  #User.create(email: email, password: '123456', role: 3, employee_id: e.id)
end

# Root
root_mail = 'root@pack.com'
e0 = Employee.create(name: Faker::Lorem.word,
                     last_name: Faker::Lorem.word,
                     birth: Time.now,
                     pay_rate: Faker::Number.number(4),
                     email: root_mail,
                     phone: Faker::Number.number(10).to_s,
                     residence: Faker::Lorem.word)
User.create(email: root_mail, password: '123456', role: 0, employee_id: e0.id)

# Admin
admin_mail = 'admin@pack.com'
e1 = Employee.create(name: Faker::Lorem.word,
                     last_name: Faker::Lorem.word,
                     birth: Time.now,
                     pay_rate: Faker::Number.number(4),
                     email: admin_mail,
                     phone: Faker::Number.number(10).to_s,
                     residence: Faker::Lorem.word)
User.create(email: admin_mail, password: '123456', role: 1, employee_id: e1.id)

# Super
super_mail = 'super@pack.com'
e2 = Employee.create(name: Faker::Lorem.word,
                     last_name: Faker::Lorem.word,
                     birth: Time.now,
                     pay_rate: Faker::Number.number(4),
                     email: super_mail,
                     phone: Faker::Number.number(10).to_s,
                     residence: Faker::Lorem.word)
User.create(email: super_mail, password: '123456', role: 2, employee_id: e2.id)

# Operator
operator_mail = 'operator@pack.com'
e3 = Employee.create(name: Faker::Lorem.word,
                     last_name: Faker::Lorem.word,
                     birth: Time.now,
                     pay_rate: Faker::Number.number(4),
                     email: operator_mail,
                     phone: Faker::Number.number(10).to_s,
                     residence: Faker::Lorem.word)
User.create(email: operator_mail, password: '123456', role: 3, employee_id: e3.id)

# Cashier
cashier_mail = 'cashier@pack.com'
e4 = Employee.create(name: Faker::Lorem.word,
                     last_name: Faker::Lorem.word,
                     birth: Time.now,
                     pay_rate: Faker::Number.number(4),
                     email: cashier_mail,
                     phone: Faker::Number.number(10).to_s,
                     residence: Faker::Lorem.word)
User.create(email: cashier_mail, password: '123456', role: 4, employee_id: e4.id)



