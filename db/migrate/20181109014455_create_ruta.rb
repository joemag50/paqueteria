class CreateRuta < ActiveRecord::Migration[5.2]
  def change
    create_table :ruta do |t|
      t.string :emisor
      t.string :destino
      t.string :distancia

      t.timestamps
    end
  end
end
