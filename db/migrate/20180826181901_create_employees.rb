class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :last_name
      t.date :birth
      t.float :pay_rate
      t.string :email
      t.string :phone
      t.string :residence

      t.timestamps
    end
  end
end
