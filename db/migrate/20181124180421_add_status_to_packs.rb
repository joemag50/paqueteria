class AddStatusToPacks < ActiveRecord::Migration[5.2]
  def change
    add_column :packs, :status, :integer
  end
end
