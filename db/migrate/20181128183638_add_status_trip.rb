class AddStatusTrip < ActiveRecord::Migration[5.2]
  def change
    add_column :trips, :status, :integer
  end
end
