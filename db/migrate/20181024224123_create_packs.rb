class CreatePacks < ActiveRecord::Migration[5.2]
  def change
    create_table :packs do |t|
      t.integer :client_id
      t.string :addressee
      t.string :address_state
      t.string :address_city
      t.string :address_pc
      t.string :address_street
      t.float :size_x
      t.float :size_y
      t.float :size_z
      t.float :weight
      t.integer :priority

      t.timestamps
    end
  end
end
