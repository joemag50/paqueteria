class CreateTrips < ActiveRecord::Migration[5.2]
  def change
    create_table :trips do |t|
      t.integer :vehiculo_id
      t.integer :pack_id, array: true
      t.integer :ruta_id

      t.timestamps
    end
  end
end
