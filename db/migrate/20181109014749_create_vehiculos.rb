class CreateVehiculos < ActiveRecord::Migration[5.2]
  def change
    create_table :vehiculos do |t|
      t.string :marca
      t.string :modelo
      t.float :max_volumen
      t.float :max_peso

      t.timestamps
    end
  end
end
