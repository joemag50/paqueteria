class Changetypeofdistance < ActiveRecord::Migration[5.2]
  def change
    change_column :ruta, :distancia, :float
  end
end
